<?php
class Log {

        private $_path="" ;
        private $_fileName="" ;
        private $_long_path="";
        static $_instance;
 
        /**
         * @param string $path can be a directory o a file path
         */
        public function __construct() {
            $this->_path = "log/";
            $this->_fileName = 'myLog.log';
            $this->_long_path= $this->_path.$this->_fileName;
 
            
        }   
 
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }


 
        /**
         * Will save the path on the give path
         * @param String $line
         */
        protected function _save($line) {
            $fhandle = fopen($this->_long_path, "a+");
            fwrite($fhandle, $line);
            fclose($fhandle);
        }
 
        /**
         * main function to add lines to the logging file
         * @param String $line
         */
        public function addLine($line){
            $line = is_array($line) ? print_r($line, true) : $line;
            $line = date("d-m-Y h:i:s") . ": $line\n";
            $this->_save($line);
        }

        
}