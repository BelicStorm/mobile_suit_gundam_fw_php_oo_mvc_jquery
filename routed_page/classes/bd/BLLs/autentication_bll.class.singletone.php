<?php
class autentication_bll extends core_bll { 
   
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function register_user($info_new_user) {
        $id=$info_new_user['email']."_".$info_new_user['username'];//generar la id de los users logeados de forma convencional
        $mail=$this->common->get_user_mail($info_new_user['email'],$id);//verificar que el mail no existe en la bd
        if(empty($mail)){//si el mail no existe entonces insertara el usuario
             $info_new_user["token"]=generate_user_id_token($info_new_user['username']);//generara un token inicial para su verificacion
             $sql="CALL create_user('".$id
                                    ."','".$info_new_user['username']
                                    ."','".$info_new_user['user_pic']
                                    ."','".$info_new_user['email']
                                    ."','".$info_new_user['password']
                                    ."','".$info_new_user['token']
                                    ."',0)";//llamaremos a un procedimiento almacenado que inserte el user
           $this->db->ejecutar($sql);//y lo ejecutaremos
           $mail=array('type'=>'alta',
                        "inputEmail"=>$info_new_user['email'],
                        "inputMessage"=>'Para activar tu cuenta pulse en el siguiente enlace',
                        "token"=>$info_new_user['token']); 
             enviar_email($mail);//enviaremos un mail al usuario con su direccion para asi poder verificar el registro
             /* var_dump($sql); */
            return "ok";
        }else{
            return "The user alredy exists";
        };
    }
    public function validate_register($old_Token){
        /* var_dump($old_Token); */
        $token=$this->common->get_user_token($old_Token,"");// obtener el id con el antiguo token
        
        $array=array(0=>array("column"=>"user_activated","argument"=>"1"));//generar el array del update
        $this->common->simple_update("user_control","user_id='".$token[0]["user_id"]."'",$array);//activar el usuario
        $this->dao->put($this->db, $this->dao->content);

        $new_token=generate_user_id_token($token[0]["user_id"]);//generar un nuevo token
        $this->common->update_token($old_Token,$new_token,"");//actualizar el token
    }
    public  function login_user($info_user) {
        $ok="";
        $id=$this->common->get_user_mail($info_user["mail"],"");//obtener el id mediante el mail
        if(sizeof($id)!=0){//si el mail existe
            $password= $this->common->get_user_password($id[0]["user_id"]); //obtener la contraseña mediante el mail
            $validation= $this->common->get_user_verificate($id[0]["user_id"]);//obtener el numero de validacion
            if($info_user["password"]==$password[0]["user_password"]){//si las contraseñas no coinciden
                $ok= "The user doesnt exist or wrong password";
            }else{
                if(!$validation["user_activated"]){//y el usuario esta activado
                    $new_token=generate_user_id_token("Login");//generamos un nuevo token
                    $this->common->update_token("",$new_token,$id[0]["user_id"]);//lo actualizamos
                    @session_start();
                    $_SESSION["user_id"]=$id[0]["user_id"];//y guardamos en sesion la id del usuario
                    $ok="ok";
                }else{
                    $ok="El usuario no esta validado. Revisa tu correo.";
                }
            }
        }else{$ok= "The user doesnt exist or wrong password";}
        
        return $ok;
    }
    public function get_user_info($to_know){
        @session_start();
        if(isset($_SESSION["user_id"])){
             $id=$_SESSION["user_id"];
        }else { 
            /* var_dump($to_know); */
            if($to_know['know']=="token"){//obtiene el token de un mail en caso de que no haya usuario logeado
                $id=$this->common->get_user_mail($to_know["mail"],"");
                $token=$this->common->get_user_token_by_id($id[0]["user_id"])[0]["user_token"];
                return $token;
            }
            if($to_know['know']=="mail"){//comprueba la existencia de un mail en caso de no haber usuario logeado
                if($this->common->get_user_mail($to_know["mail"],"")){
                    return "0";
                }
                return "1";
            }
            return "No Loged User"; 
        }

        switch ($to_know['know']) {
            case 'all':
                return $this->common->get_all_info($id);;
            break;
        }
        
    }
    public function update_controller($options){ 
        /* var_dump($id); */
        switch ($options["option"]) {
            case 'pass':
                $token=$options["token"];
                $id=$this->common->get_user_token($token,"");
                return $this->common->update_password($id[0]["user_id"],$options["new_pass"]);
                break;
            case 'profile_pass':
                return $this->common->update_password($options["id"],$options["new_pass"]);
                break;
            case 'profile_avatar':
                @session_start();
                if ($_SESSION["new_file"]!="No") {
                    $new_url_avatar=upload_image::upload_picture("user_avatar");
                    return $this->common->update_avatar($options["id"],$new_url_avatar);
                }else{
                    return "mal";
                }
                break;
            
            default:
                # code...
                break;
        }
    }
}
