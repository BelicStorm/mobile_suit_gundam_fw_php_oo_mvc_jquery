<?php
//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page/';
define('SITE_ROOT', $path);
define('UPDATE_ROOT', SITE_ROOT."media/");
define('SITE_PATH', "http://".$_SERVER['HTTP_HOST'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page/');
define('Upload_files_ROOT', SITE_PATH."media/");
define('Amigable_PATH', "http://".$_SERVER['HTTP_HOST'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page');

//Classes Path
    define('Classes_ROOT', SITE_ROOT."classes/");
        //BD_Classes
            define('bd_Root',Classes_ROOT."bd/");
            define('bd_bll',bd_Root."BLLs/");
            define('bd_conf',bd_Root."conf_and_connection/");
            define('bd_utils',bd_Root."utils/");
            define('bd_DAO',bd_Root."DAO/");
        //BD_Classes
//Classes Path    
//Controller Path
    define('Controller_ROOT', SITE_ROOT."controller/");
        //error controler
        define('Controller_Error', Controller_ROOT."error_controller/");
        //home controller
        define('Controller_Home', Controller_ROOT."home_controller/");
        //contact controller
        define('Controller_Contact', Controller_ROOT."contact_controller/");
        //login controller
        define('Controller_Login', Controller_ROOT."login_controller/");
        //login controller
        define('Controller_About_us', Controller_ROOT."about_us_controller/");
//Controller Path
//Views Path
    define('Views_ROOT', SITE_PATH."view/");
        define('Views_Utils', Views_ROOT."utils/");
        define('Views_CSS', Views_ROOT."css/");
        define('Views_JS', Views_ROOT."js/");
        define('Views_inc', Views_ROOT."inc/");
        define('Views_Modules', Views_ROOT."modules/");
            define('Views_Modules_contact', Views_Modules."contact/js/");
            define('Views_Modules_home', Views_Modules."home/js/");
            define('Views_Modules_login', Views_Modules."login/js/");
//Views Path

//utils Path
    define('Utils_ROOT', SITE_ROOT."utils/");

 define('URL_AMIGABLES', TRUE);