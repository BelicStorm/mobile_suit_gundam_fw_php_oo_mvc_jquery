<?php
class Controller {
  public static function CreateView($view_name){
    if (file_exists("./View/modules/$view_name/$view_name.html")) {
      require_once("./View/inc/top_page_ms.html");
      require_once("./View/inc/header.html");
      require_once("./View/inc/menu.html");
      require_once("./View/modules/$view_name/$view_name.html");
      require_once("./View/inc/footer.html");
      require_once("./View/inc/bottom_page.html");
    }
  }
   public static function CreateSubView($forlder_name,$view_name){
    if (file_exists("./View/modules/$forlder_name/$view_name.html")) {
      require_once("./View/inc/top_page_ms.html");
      require_once("./View/inc/header.html");
      require_once("./View/inc/menu.html");
      require_once("./View/modules/$forlder_name/$view_name.html");
      require_once("./View/inc/footer.html");
      require_once("./View/inc/bottom_page.html");
    }
  }
}
?>
