window.onload=function(){
        $(document).on( "click","#btn-general-lang", function(){
        var content = $("#lang").val();
        //console.log(content);
        cambiarIdioma(content);
        });
    };
function cambiarIdioma(lang){
  //console.log("Fichero cargado"+' menu_'+lang+'.json');
  // Habilita las 2 siguientes para guardar la preferencia.
  lang = lang || sessionStorage.getItem('app-lang') || 'es';
  sessionStorage.setItem('app-lang', lang);
  url="http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page/";
  $.ajax({ 
    type: 'GET', 
    url: url+'view/inc/lang/menu/menu_'+lang+'.json', 
    data: { get_param: 'value' }, 
    dataType: 'json',
    success: function (data) { 
        //console.log(data);
        var elems = document.querySelectorAll('[data-tr]');
        for (var x = 0; x < elems.length; x++) {
            elems[x].innerHTML = data.hasOwnProperty(lang)
                ? data[lang][elems[x].dataset.tr]
                : elems[x].dataset.tr;
        }
    }
  });
}
