function move_to(url){
    window.location.href=(url);
}
create_menu= new Promise(function(resolve) {
    url=amigable("?module=login&function=get_user_info");
    know="know=all";
    default_append='<li><a><span data-tr="Inicio" id="menu_home" name="menu_home"></a></li>'+
                    '<li><a><span data-tr="tienda_productos" id="menu_shop" name="menu_shop"></a></li>'+
                    '<li><a><span data-tr="Conocenos" id="menu_know" name="menu_know"></a></li>'+
                    '<li><a><span data-tr="Contacto" id="menu_contact" name="menu_contact"></a></li>';
    shop_append='<li><a><span data-tr="Inicio" id="menu_home" name="menu_home"></a></li>'+
                    '<li><a><span data-tr="tienda_productos" id="menu_shop" name="menu_shop"></a></li>'+
                    '<li><a><span data-tr="Conocenos" id="menu_know" name="menu_know"></a></li>'+
                    '<li><a><span data-tr="Contacto" id="menu_contact" name="menu_contact"></a></li>'+
                    '<li><a><span data-tr="MS" id="menu_ms" name="menu_ms"></a></li>';
    admin_append='<li><a><span data-tr="Inicio" id="menu_home" name="menu_home"></a></li>'+
                    '<li><a><span data-tr="tienda_productos" id="menu_shop" name="menu_shop"></a></li>'+
                    '<li><a><span data-tr="Conocenos" id="menu_know" name="menu_know"></a></li>'+
                    '<li><a><span data-tr="Contacto" id="menu_contact" name="menu_contact"></a></li>'+
                    '<li><a><span data-tr="MS" id="menu_ms" name="menu_ms"></a></li>'+
                    '<li><a><span data-tr="All_Products" id="menu_products" name="menu_products"></a></li>';
    $.ajax({
        url : url,
        type: 'POST',
        data: know,
        dataType: 'json',
    }).done(function(response){ //
        if(response=="No Loged User"){
            $("#login-handler").append('<label id="login">Login</label>');
            $('#menu_handler').append(default_append);
        }else{
            console.log(response);
            $('#user_avatar').append('<img id="avatar" src="'+response[0].user_avatar+'"> <span id="user_name"></span>');
            $("#login-handler").append('<label id="logout">Logout</label>'); 
            switch (response[0].type) {
                case "1":
                    $('#menu_handler').append(admin_append);
                    break;
                case "2":
                    $('#menu_handler').append(default_append);
                    break;
                case "3":
                    $('#menu_handler').append(shop_append);
                    break;
            }
        }
    });
    resolve(0);  
});

$(document).ready(function(){
    $('#menu_handler').on("click","#menu_home",function(){move_to(amigable("?module=home&function=doSomething"));})
    $(document).on("click","#login",function(){move_to(amigable("?module=login&function=doSomething"));})
    $('#menu_handler').on("click","#menu_know",function(){move_to(amigable("?module=about_us&function=doSomething"));})
    $(document).on("click",'#avatar',function(){move_to(amigable("?module=login&function=charge_profile"));})
    
    create_menu.then(function(resolve){
        cambiarIdioma();
    })

})