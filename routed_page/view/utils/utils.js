var home_ajax = function(url,data){
    return new Promise(function(resolve){
        $.ajax({
            url : url,
            type: 'POST',
            data: data,
            dataType: 'json',
        }).done(function(response){ //
            /* console.log(response); */
            resolve(response);
        });  
        
    })
}
var profile_pics_get_api = function(){
    return new Promise(function(resolve){ //obtiene las imagenes necesarias para el registro
       var response="";
        $.ajax({
            url : "http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/ms_images.json",
            type: 'get',
        }).done(function(response){ //
            //console.log(response[0].url);
             resolve(response.profile_pics);
        } )/*.fail(function(){
            home_ajax("http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/ms_images.json","").then(function(response2){
                resolve(response2.profile_pics);
            })
        }) */
    });
}

function amigable(url) {
    var link="";
    url = url.replace("?", "");
    url = url.split("&");
    cont = 0;
    for (var i=0;i<url.length;i++) {
    	cont++;
        var aux = url[i].split("=");
        if (cont == 2) {
        	link +=  "/"+aux[1]+"/";	
        }else{
        	link +=  "/"+aux[1];
        }
        
    }
    return "http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page" + link;
}

function delete_session_image(){
  $.ajax({
      url : amigable("?module=upload_image&function=pre_upload_one_image&aux=delete"),
      type: 'POST',
      data: { get_param: 'value' },
      dataType: 'json',
  }).done(function(response){ //
      /* console.log(response); */
      
  }); 
}

$("#mydropzone").ready(function(){//dropzone filters
    if($("#mydropzone").val()!=undefined){
            Dropzone.autoDiscover = false;
        // Dropzone class:
        var myDropzone = new Dropzone("div#mydropzone", {
            url:amigable("?module=upload_image&function=pre_upload_one_image&aux=upload"),//controlador de subidas
            maxFilesize: 5,//peso maximo del fichero
            maxFiles: 1,//numero maximo de imagenes por subida
            uploadMultiple: false,//prohibir la subida de multiples archivos
            addRemoveLinks: true,//añadir el delete files de forma grafica
            autoProcessQueue: true,//control de autosubida de archivos
            dictResponseError: 'Server not Configured',
            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",//extensiones permitidas
            init:function(){
            var self = this;
            /* console.log(this.files.length); */
            if(this.files.length==0){delete_session_image();} //si no hay imagenes borra la sesion que las almacena y del servidor
            // config
            self.options.addRemoveLinks = true;
            self.options.dictRemoveFile = "Delete";//Texto del boton de borrado

            self.on("addedfile", function (file) { //cuando se añade un fichero
                console.log('new file added ', file);
            });

            self.on("sending", function (file) {//cuando se envia
                console.log('upload started', file);
                $('.meter').show();
            });
            
            self.on("totaluploadprogress", function (progress) { //progreso total de la subida
                console.log("progress ", progress);
                $('.roller').width(progress + '%');
            });

            self.on("queuecomplete", function (progress) {//subida completa
                $('.meter').delay(999).slideUp(999);
            });
            
            self.on("removedfile", function (file) {//fichero borrado
                console.log(file);
                delete_session_image(); //si se borra la imagen del dropzone, borra la imagen de la sesion que lo almacena y del servidor
            });
            }
        });
    }
})