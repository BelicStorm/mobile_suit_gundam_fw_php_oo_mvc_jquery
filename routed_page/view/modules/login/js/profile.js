function load_pages(option){ //carga las paginas dependiendo de cual este guardada en localstorage
        $(".profile_user_name").empty();
        $(".profile_user_mail").empty();
        $(".profile_user_avatar").empty();
         $("#tracking_social").empty();
         $("#purchase_history").empty();
    switch (option){
        case '#my_acount':
            /* console.log('cuenta'); */
            localStorage.setItem("profile_tab", option);
            load_my_acount();
        break;
        case '#update_profile':
            /* console.log('update'); */
            localStorage.setItem("profile_tab", option);
        break;
        case '#tracking_social':
            /* console.log('social'); */
            localStorage.setItem("profile_tab", option);
        break;
        case '#purchase_history':
            /* console.log('social'); */
            localStorage.setItem("profile_tab", option);
        break;
        default:
            /* console.log('default'); */
            localStorage.setItem("profile_tab", "#my_acount");
            load_my_acount();
        break;
    }

    var activeTab = localStorage.getItem('profile_tab');
    if(activeTab){
		$('#profile_tabs a[href="' + activeTab + '"]').tab('show');
	}
}
//validate functions
    function validate_new_pass(){
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(document.getElementById('new_pass').value)){
            document.getElementById('e_Reg_new_password').innerHTML = "Tu contraseña tiene que contener al menos 8 caracteres,una mayuscula, una minuscula y un caracter especial";
                return 1;
            }
            if(document.getElementById('re_new_pass').value==""){
                document.getElementById('e_Reg_new_password').innerHTML = "";
                return 1;
                }
            document.getElementById('e_Reg_new_password').innerHTML = "";
            return;
    }
    function confirm_new_pass(){
        if (document.getElementById('new_pass').value!=document.getElementById('re_new_pass').value||document.getElementById('new_pass').value==""){
            document.getElementById('e_Reg_new_confirm_password').innerHTML = "Tus contraseñas no coinciden";
                return 1;
            }
            document.getElementById('e_Reg_new_confirm_password').innerHTML = "";
            return 0;
    }
    function habilitar_boton_update(ok){
        if(ok!=1){
            $('#update_profile_data').attr("disabled", false);//habilita boton
        }else{
            $('#update_profile_data').attr("disabled", true);//desabilita boton
        }
    }
//validate functions
function load_my_acount(){
    url=amigable("?module=login&function=get_user_info");
    home_ajax(url,"know=all").then(function(data_profile_my_acount){
        console.log(data_profile_my_acount);
        $(".profile_user_name").append(data_profile_my_acount[0].user_name);
        $(".profile_user_mail").append(data_profile_my_acount[0].user_mail);
        $(".profile_user_avatar").append("<img  src='"+data_profile_my_acount[0].user_avatar+"'>");
    })
}
function update_profile_data_controller(new_profile_data){
     url=amigable("?module=login&function=update_user&aux=out&aux2=profile_pass");
    home_ajax(url,new_profile_data).then(function(data_profile_my_acount){
        console.log(data_profile_my_acount);
    })
}
$(document).ready(function(){
    load_pages(localStorage.getItem("profile_tab"));//carga las paginas del perfil
   $('#profile_tabs').on('click','a',function (e) { // cambia las paginas cuando el usuario clike en ellas
       /* console.log($(this).attr('href')); */
        load_pages($(this).attr('href'));
    });
    var ok2 = 1;
    $('#update_profile_data').attr("disabled", true);//desabilita boton
    $('#new_pass').on('focus blur change', function() {
            ok2=validate_new_pass();
            habilitar_boton_update(ok2);
    });
    $('#re_new_pass').on('focus blur change', function() {
            ok2=confirm_new_pass();
            habilitar_boton_update(ok2);
    });
    $('#update_profile_data').on('click', function(){
        new_profile_data = {new_pass:$('#new_pass').val(), 
                            re_new_pass:$('#re_new_pass').val()};
        update_profile_data_controller(new_profile_data);
    });
    //cambiar foto de perfil
        $('#update_avatar').on('click',function (e) { //cuando se pulse en actualizar avatar
        url1=amigable("?module=login&function=update_user&aux=out&aux2=profile_avatar");
            home_ajax(url1,"").then(function(resolve){
                //subir la imagen al servidor
                if(resolve==true){
                        //recarga la pagina
                        setTimeout(() => {
                                load_pages("#my_acount");
                                document.location.reload(true);
                        }, 100);
                }
            });
        });
    //cambiar foto de perfil
})