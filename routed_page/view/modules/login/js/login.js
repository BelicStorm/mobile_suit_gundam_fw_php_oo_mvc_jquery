$(function() {
    $("#register_form").fadeOut(100);
    $("#forgot-form").fadeOut(100);
    $('#login-form-link').click(function(e) {
        $("#register_form").fadeOut(100);
        $("#forgot-form").fadeOut(100);
		$("#login_form").delay(100).fadeIn(100);
        $('#forgot-form-link').removeClass('active');
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register_form").delay(100).fadeIn(100);
 		$("#login_form").fadeOut(100);
        $("#forgot-form").fadeOut(100);
        $('#forgot-form-link').removeClass('active');
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
    $('#forgot-form-link').click(function(e) {
		$("#forgot-form").delay(100).fadeIn(100);
 		$("#login_form").fadeOut(100);
        $("#register_form").fadeOut(100);
        $('#register-form-link').removeClass('active');
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});


})
//validate functions
    function white_space(form){//valida que no hay espacios en blanco en el formulario
        var op = true;
        /* $("input:radio").each(function(){
            var name = $(this).attr("name");
            if($("input:radio[name="+name+"]:checked").length == 0){
                op = false;
            }
        }); */
        $("#"+form).find(':input').each(function() {
            var elemento= this;
            
            if(elemento.value.length==0){
                alert('Rellena los campos');
                op=false;
                if(op==false){
                    return op;
                }
            }
            
        });
        return op;
    }
    function validate_name(){
        if (!/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/.test(document.getElementById('reg_username').value)){
            document.getElementById('e_Reg_username').innerHTML = "Tu nombre tiene que tener de 4 a 20 caracteres y no incluir _ o .";
                return 1;
            }
            document.getElementById('e_Reg_username').innerHTML = "";
            return 0;
    }
    function validate_mail(mail,error){
        if (!/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(document.getElementById(mail).value)){
            document.getElementById(error).innerHTML = "Tu Mail no es valido";
                return 1;
            }
            document.getElementById(error).innerHTML = "";
            return 0;
    }
    function validate_pass(){
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(document.getElementById('reg_password').value)){
            document.getElementById('e_Reg_password').innerHTML = "Tu contraseña tiene que contener al menos 8 caracteres,una mayuscula, una minuscula y un caracter especial";
                return 1;
            }
            document.getElementById('e_Reg_password').innerHTML = "";
            return 0;
    }
    function confirm_pass(){
        if (document.getElementById('reg_password').value!=document.getElementById('reg_confirm_password').value){
            document.getElementById('e_Reg_confirm_password').innerHTML = "Tus contraseñas no coinciden";
                return 1;
            }
            document.getElementById('e_Reg_confirm_password').innerHTML = "";
            return 0;
    }
//validate functions
function load_register_avatar(){
    profile_pics_get_api().then(function(result){ //pinta en el formulario de registro las imagenes de perfil
        /* console.log(result[0].url); */
        content="";
        if(result!='no'){
            $.each(result, function(index, list) {
                if(index!=0){
                    content += '<div id="profile_pic"><img src="'+list.url+'"></div>';
                    content += '<input type="radio" name="user_pic" value="'+list.url+'">';
                }else{
                    content += '<div id="profile_pic"><img src="'+list.url+'"></div>';
                    content += '<input type="radio" name="user_pic" value="'+list.url+'" checked>';
                }
            });
        }else{
                content += '<span>Sin imagenes disponibles</span>';
        }
        //console.log(content);
        $('.user_select_avatar').empty();
        $('.user_select_avatar').append(content);
     })
}
function register(info_new_user){
    url=amigable("?module=login&function=validate_register");
    home_ajax(url,info_new_user).then(function(resolve){
        console.log(resolve);
       if(resolve=="The user alredy exists"){
            document.getElementById('e_Reg_error').innerHTML = resolve;
        }else{
            document.getElementById('e_Reg_error').innerHTML = "";
            url=amigable("?module=home&function=doSomething");
            window.location.href=(url);
        }
            
       
        
    })
}
function login(info_user){
    url=amigable("?module=login&function=validate_login");
    home_ajax(url,info_user).then(function(resolve){
        console.log(resolve);
       if(resolve=="ok"){
           document.getElementById('e_log_error').innerHTML = "";
            url=amigable("?module=home&function=doSomething");
            window.location.href=(url);
        }else{
            document.getElementById('e_log_error').innerHTML = resolve;
        }
    })
}
function forgot_password(user_mail){
    //info para el get user info
    url=amigable("?module=login&function=get_user_info");
    user_mail=user_mail.split("=");
    mail=user_mail[1];
    options="know=mail&mail="+mail;
    //info para recuperar la contraseña
    url2=amigable("?module=login&function=forgot_pass");
    options2="mail="+mail;
    home_ajax(url,options).then(function(resolve){
        console.log(resolve);
        if(resolve!="1"){
            home_ajax(url2,options2).then(function(resolve){
               document.getElementById("error_forgot").innerHTML = "La nueva contraseña ha sido enviada al mail indicado. No se efectuara el cambio hasta que se verifique en el Mail Indicado";
            });
        }else{
            document.getElementById("error_forgot").innerHTML = "El Mail indicado no existe";
        }
        
    });
}
/* function setSession(authResult) {
  var expiresAt = JSON.stringify(
    authResult.expiresIn * 1000 + new Date().getTime()
  );
  localStorage.setItem('access_token', authResult.accessToken);
  localStorage.setItem('id_token', authResult.idToken);
  localStorage.setItem('expires_at', expiresAt);
}
function handleAuthentication(webAuth) {
    webAuth.parseHash(function(err, authResult) {
        if (authResult && authResult.accessToken && authResult.idToken) {
            webAuth.client.userInfo(authResult.accessToken, function(err, profile) {
                url=amigable("?module=login&function=social_signup");
                home_ajax(url,profile).then(function(resolve){
                   
                }) 
            });
        } else if (err) {
        }
    });
} */
$(document).ready(function(){
    ok=0;
    /*  var webAuth = new auth0.WebAuth({
        domain: 'daw1-gundam.eu.auth0.com',
        clientID: 'X0jS6WnBHnyqnGosCnScxJgllysruma5',
        redirectUri: 'http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/routed_page/login/doSomething/',
        audience: 'https://' + 'daw1-gundam.eu.auth0.com' + '/userinfo',
        responseType: 'token id_token',
        scope: 'openid profile',
        leeway: 60
    }); */
    load_register_avatar(); 
    //validations  
        $('#reg_username').on('focus blur change', function() {
            ok=validate_name();
        });
        $('#reg_email').on('focus blur change', function() {
            ok=validate_mail("reg_email",'e_Reg_email');
        });
        $('#reg_password').on('focus blur change', function() {
            ok=validate_pass();
        });
        $('#reg_confirm_password').on('focus blur change', function() {
            ok=confirm_pass();
        });

    //validations
    //social actions
        $('#register_form').keydown(function(e) {
                var key = e.which;
                if (key == 13) {
                    if(white_space('register_form')){
                        if(ok==0){
                            console.log($('#register_form').serialize());
                            register($('#register_form').serialize())
                            /* console.log($('#register_form').serialize()); */
                        }   
                    }
                }
            }); 
        $("#register-submit").on('click',function(){
            if(white_space('register_form')){
                if(ok==0){
                    register($('#register_form').serialize())
                    /* console.log($('#register_form').serialize()); */
                }  
            }
        }); 

        $('#login_form').keydown(function(e) {
                var key = e.which;
                if (key == 13) {
                    if(white_space('login_form')){
                            console.log($('#login_form').serialize());
                            login($('#login_form').serialize())
                    }
                }
            }); 
        $("#login_submit").on('click',function(){
            if(white_space('login_form')){
                    login($('#login_form').serialize())
                    /* console.log($('#login_form').serialize());  */
            }
        });
         
        $(document).on('click',"#logout",function(){
            url=amigable("?module=login&function=logout");
            home_ajax(url,"").then(function(resolve){
                if(resolve=="ok"){
                    window.location.href=amigable("?module=home&function=doSomething");
                }
            })
        });
        $("#forgot-submit").on('click',function(){
            var user_mail=[];
            if(white_space('forgot-form')){
                if(validate_mail('mail_forgP','error_forgot')!=1){
                    /* console.log($('#forgot-form').serialize());  */
                    user_mail=$('#forgot-form').serialize();
                    forgot_password(user_mail);
                }
            }
        });
    //social actions


/* $(document).on('click','#login_auth0',function(e){   
    e.preventDefault();
    webAuth.authorize();
});

    handleAuthentication(webAuth); */

    


})
