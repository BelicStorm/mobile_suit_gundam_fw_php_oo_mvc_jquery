<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/pruebas_orm/bd/';

define('SITE_ROOT', $path);
define('MODEL_PATH', SITE_ROOT . 'conf_and_connection/');

require(MODEL_PATH . "db.class.singletone.php");
require(SITE_ROOT . "DAO/DAO.class.singletone.php");

class profile_bll { 
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_all_users() {
        $arrArgument=$this->dao->select("*","usuarios");
        return $this->dao->get($this->db, $this->dao->content);
    }
    public function custom_test_select($array) {
        //prepara la sentencia y la almacenas en la clase
        $argument=$this->dao->select($array['rows'],$array['table']);
        $argument=$argument . $this->dao->where_argument($array['where']);

        //ejecutas las sentencia almacenada en la clase
        return $this->dao->get($this->db);
    }
    public function custom_test_select2($array) {
        //prepara la sentencia y la almacenas en la clase
        $argument=$this->dao->select($array['rows'],$array['table']);
        $argument=$argument . $this->dao->join_argument($array['inner']);
        $argument=$argument . $this->dao->on_argument($array['on']);
        $argument=$argument . $this->dao->where_argument($array['where']);
        $argument=$argument . $this->dao->and_argument($array['and']);

        //ejecutas las sentencia almacenada en la clase
        return $this->dao->get($this->db);
    }
    public function select_user($name) {
        $arrArgument=$this->dao->select("*","usuarios");
         $arrArgument=$this->dao->where_argument("user_name='$name'");
        return $this->dao->get($this->db, $this->dao->content);
    }

}