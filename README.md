# Mobile_suit_Gundam_PHPOOMVC_JQUERY


## **Resumen**
- Proyecto que emula una pagina de compraventa de productos ficticios de la serie Mobile Suit Gundam del periodo Universal Century.

## **Tecnologias Usadas**
1. Json Server
2. JqWidgets
	1. Datatable
	2. Autocomplete

## **Links**

[JqueryInput Autocomplete](https://www.jqwidgets.com/jquery-widgets-demo/demos/jqxinput/index.htm#demos/jqxinput/defaultfunctionality.htm)

**Extras**
