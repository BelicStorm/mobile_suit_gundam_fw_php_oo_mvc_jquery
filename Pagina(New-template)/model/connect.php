<?php
    class conect_disconect{
        public static function con(){
            $conexion = mysqli_connect('localhost', 'root');
            if (mysqli_connect_errno() !=0){
                //echo 'Mensaje:'.mysqli_connect_error();
                exit();
                
            }
            mysqli_select_db($conexion, "mobile_suit_db");
            if (mysqli_errno($conexion) !=0){
                //echo "mensaje:".mysqli_error($conexion);
                mysqli_close($conexion);
                exit();
            }
            return $conexion;
        }
        public static function close($conexion){
            mysqli_close($conexion);
        }
    }
?>