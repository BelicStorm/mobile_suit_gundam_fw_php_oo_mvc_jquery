function ajax_shop(url_send){
    return JSON.parse(
        $.ajax({
            type: "get",
            url: url_send,
            dataType: 'json',
            async:false,
        success: function(data)
        {
            
            ////console.log(data);
            return data;
        }
    }).responseText);
}
function load_page(){
    return new Promise (function(){
        url = "index.php?page=shop";
    $( location ).attr("href", url);
    })
    
}
function print_shop(data_send){ //imprime un lista o los detalles de los productos
   sessionStorage.setItem("content",data_send);
    load_page();  
}

function list_shop_view(data_send){
    var content = "";
      $('#list_shop').empty();
        $.each(data_send, function(index, list) {
            content +="<div class='item_pagination'>";
            content += '<h4>'+list.modelo+''+list.ms_name+'</h4>';
            /*content=content+ "<div class='social like "+list.Modelo+" "+list.Nombre+"' ></div>";*/
            content += "<a class='detailed_veiw_shop "+list.ms_name+"'><i class='fa fa-search'></i> </a>";
            content=content+ "<i class='fa fa-plus' id='"+list.modelo+" "+list.ms_name+"'></i></div>";
        });
    return content;
    
}

function detailed_shop_view(data_send) {
    var content ="";
    var url = "";
    var ajax_image = new Promise(function(resolve, reject) {
        url_send=ajax_shop("http://localhost:3000/"+data_send[0].modelo+"/"+data_send[0].ms_name);
        console.log(url_send);
        /* console.log(JSON.stringify(url_send.MS[0].id)); */
        if(url_send){
           /* console.log(data_send[0].modelo);
            console.log(data_send[0].ms_name);*/
            
            resolve(url_send.url);
        }
    });
    ajax_image.then(function(result) {
            url=result; // "Stuff worked!"
            $.each(data_send, function(index, list) {
                content += '<h3>'+list.modelo+list.ms_name+'<i class="fa fa-plus" id="'+list.modelo+' '+list.ms_name+'"></i></h3>';
                content +="<div class='social like "+list.modelo+" "+list.ms_name+"'></div>";
                content += "<table id='table_detailed_shop'>";
                content += "<tr><td rowspan='12'><img src='"+url+"'></td>";
                content += "<td width=80><b>Productor</td>";
                content += '<td width=80><b>Peso Total</td>';
                content += '<td width=80><b>Peso Vacio</td>';
                content += '<td width=80><b>Numero de Pilotos</td><tr>';
                content += '<tr><td width=80>'+list.bando+'</td>';
                content += '<td width=80>'+list.peso_Total+'</td>';
                content += '<td width=80>'+list.peso_Vacio+'</td>';
                content += '<td width=80>'+list.pilots+'</td><tr>';
                content += '<tr><td width=80><b>Energia</td>';
                content += '<td width=80><b>Encendido</td>';
                content += '<td width=80><b>Rango delo sensor</td>';
                content += '<td width=80><b>En produccion desde</td><tr>';
                content += '<tr><td width=80>'+list.energia+'</td>';
                content += '<td width=80>'+list.encendido+'</td>';
                content += '<td width=80>'+list.rango_Sensor+'</td>';
                content += '<td width=80>'+list.production_date+'</td><tr>';
                content += '<tr><td width=80><b>Velocidad</td>';
                content += '<td width=80><b>Tamaño total</td>';
                content += '<td width=80><b>Tamaño de la unidad principal</td>';
                content += '<td width=80><b>Precio por unidad</td><tr>';
                content += '<tr><td width=80>'+list.velocidad+'</td>';
                content += '<td width=80>'+list.tamaño_Total+'</td>';
                content += '<td width=80>'+list.tamaño+'</td>';
                content += '<td width=80>'+list.price_per_unit+'</td><tr>';
                
            });
            //console.log(url);
            content += "</table>"; 
            //console.log(content);
            sessionStorage.setItem("content", content);
            $('#list_shop').append(sessionStorage.getItem("content"));
        
    });
      return content;        
}



$(document).ready(function(){
    if(sessionStorage.getItem("content")){
        $('#list_shop').empty();
        var data = ajax_shop("module/shop/controller/shop_controller.php"+sessionStorage.getItem("content"));
        if(data.length <= 1){
                $('#list_shop').append(detailed_shop_view(data));
        }else{
            $("#paginated_list_shop").bootpag({
                total: Math.round(data.length/3),
                page: 1,
                maxVisible: 3,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {
                $('#list_shop').append(list_shop_view(data));
            })
        }
    }
});

$(document).ready(function(){//genera la url correspondiente a cada tipo de busqueda
    $(".search_autoguess").on('click',function(){ //busqueda
        sessionStorage.removeItem('content');
        var bando = $('#bandos').val();
        var modelo = $('#modelos').val();
        var name= "";

        //dependiendo del tipo de busqueda el jqximput setea el value de una forma u otra
        if($('#jqxInput').val().value)//busqueda autocompletada
            {name=$('#jqxInput').val().value}
        else{name=$('#jqxInput').val()}//busqueda sin parametros de autocompletar


        var search = [bando,modelo,name ];
        if(search[0]){
            if (search[1]) {
                if(search[2]){
                    ////console.log("bando + modelo + nombre");
                    var data = "?op=bando_modelo_name&bando="+bando+"&modelo="+modelo+"&name="+name;
                    //console.log( data );
                    print_shop(data);
                }else{
                    ////console.log("bando + modelo");
                    var data = "?op=bando_modelo&bando="+bando+"&modelo="+modelo;
                    ////console.log( data );
                    print_shop(data);
                }
            }else if(search[2]){
                ////console.log("bando + nombre");
                var data = "?op=bando_name&bando="+bando+"&name="+name;
                //console.log( data );
                print_shop(data);
            }else{
                ////console.log("solo bando");
                var data = "?op=bando&bando="+bando;
                //console.log( data );
                print_shop(data);
            }
        }else if(search[2]){
            ////console.log("solo like el nombre");
            var data = "?op=name&name="+name;
            //console.log( data );
            print_shop(data);
        }else{
            ////console.log("todo");
           var data = "?op=all";
           //console.log( data );
           print_shop(data);
        }
        
    });
    $("#menu_shop").on('click',function(){// menu -> Productos
        sessionStorage.removeItem('content');
         ////console.log("todo");
         var data = "?op=all";
         //console.log( data );
         print_shop(data);
    });
    $(".offer").on('click',function(){ //para las ofertas *Futuro*
        sessionStorage.removeItem('content');
        //console.log($(this).attr('class').split(' ')[1]);

    });
    $(document).on('click',".detailed_veiw_shop",function(){//ver articulo de base de datos
       var name= $(this).attr('class').split(' ')[1];
       var data = "?op=name&name="+name;
       print_shop(data);

   });
    
});
    
    