<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/Pagina(New-template)/';
include($path . "bd/utils/common.inc.php");
include($path . "utils/upload_images.php");
$path_bbl=$path."bd/BLLs/";
switch($_GET["op"]){
    case "user_profile_data":
        @session_start();
        if(isset($_SESSION["user"])){
            $user_name=$_SESSION["user"];
            echo json_encode(load_bll($path_bbl,"profile_bll","select_user","$user_name"));
        }
    break;
    case "update_control"://actualizar perfil
         @session_start();
        if(isset($_SESSION["user"])){//si no hay ningun usuario logeado no entra
            $array = array('type'=>"","user_mail"=>"","user_pass"=>"","error"=>"");//array de control de errores
            $array_sentence = array();//array para la sentencia de update
            $array_to_send = array();//array para enviar al bll
            $user_name=$_SESSION["user"];//usuario
            $old_data=load_bll($path_bbl,"profile_bll","select_user","$user_name");//antiguos datos del perfil
            
            if ($_POST['mail']!=""&&$_POST['new_pass']!="") {//si por alguna razon entran estando los campos vacios saldra
                echo json_encode("ok");
            }
        
            if($_POST['mail']!=""){//control del campo mail
                if ($_POST['mail']!=$old_data[0]['user_mail']) {//si es diferente al antiguo mail
                    $array['user_mail']=$_POST['mail'];//Guarda el mail en el array de control
                }else{
                        $array["type"]='e_Reg_new_email';//guarda en el array de control el error
                        $array["error"]="El nuevo mail tiene que ser diferente al antiguo";
                        echo json_encode($array);
                }
            } 
            if($_POST['new_pass']!=""){//control del campo password
                if (password_verify($_POST['new_pass'], $old_data[0]['user_pass'])) {//si es diferente a la antigua contraseña
                    $array["type"]='e_Reg_new_confirm_password';//guarda en el array de control el error
                        $array["error"]="La nueva contraseña no puede ser igual a la anterior";
                        echo json_encode($array);
                } else {//Guarda la nueva contraseña en el array de control
                     $array['user_pass']=password_hash($_POST['new_pass'], PASSWORD_DEFAULT);
                }
            } 
           if($array['error']==""){
                foreach ($array as $key => $value) { 
            //Si no ha ocurrido ningun error desde el array de control se montara el array con los campos
                if ($key!="error"&&$value!="") {
            /* $array=array(1=>array("column"=>,"argument"=>)...); El key del array es la columna y el value el argumento del update*/
                    array_push($array_sentence,array("column"=>"$key","argument"=>"$value"));
                }
            }
            array_push($array_to_send,$user_name,$array_sentence);//Por ultimo se monta el array de envio con el usuario y la sentencia 
            load_bll($path_bbl,"profile_bll","update_profile",$array_to_send);
            
            echo json_encode("ok");
           }

        }
    break;
    case "update_avatar"://si hay imagen actualiza la tabla con su direccion
        @session_start();
        if ($_SESSION["new_file"]!="No") {
            $array_to_send=array();
            $new_url_avatar=upload_picture("user_avatar");
            $array_sentence=array(0=>array("column"=>"avatar","argument"=>$new_url_avatar));
            array_push($array_to_send,$_SESSION["user"],$array_sentence);
            echo json_encode(load_bll($path_bbl,"profile_bll","update_profile",$array_to_send));
        }else{
            echo json_encode("mal");
        }
    break;
    case "media_profile":
         @session_start();
        if(isset($_SESSION["user"])){
            $user_name=$_SESSION["user"];
            echo json_encode(load_bll($path_bbl,"profile_bll","select_media","$user_name"));
        }
    break;
    case "purchase_history":
    @session_start();
        if(isset($_SESSION["user"])){
            $user_name=$_SESSION["user"];
            $table_compra="historial_compra_of_".$user_name;
            $compra=(load_bll($path_bbl,"profile_bll","select_all","$table_compra"));
            echo json_encode($compra);
        }
    break;
    case "product_history":
     @session_start();
        if(isset($_SESSION["user"])){
             $user_name=$_SESSION["user"];
            $table_productos="historial_productos_of_".$user_name;
            $producto=(load_bll($path_bbl,"profile_bll","select_all","$table_productos"));
            echo json_encode($producto);
        }
    break;
    case "all_purchase_history":
         @session_start();
        if(isset($_SESSION["user"])){
            $user_name=$_SESSION["user"];
            $table_compra="historial_compra_of_".$user_name;
            $table_productos="historial_productos_of_".$user_name;
            $compra=(load_bll($path_bbl,"profile_bll","select_all","$table_compra"));
            $producto=(load_bll($path_bbl,"profile_bll","select_all","$table_productos"));
            /* var_dump($compra." ".$producto); */
            $historial_de_compra=array();
            foreach ($compra as $key1 => $value1) {
                $historial_de_compra[$key1]=$value1;
                $i=0;
                foreach ($producto as $key2 => $value2) {
                    if($value1["id_compra"]==$value2["id_compra"]){
                        $historial_de_compra[$key1][$i]=$value2;
                        $i=$i+1;
                    }
                }
            }
             /* var_dump($historial_de_compra);  */
        }
        echo json_encode($historial_de_compra);
    break;
}


?>