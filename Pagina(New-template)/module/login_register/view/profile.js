var profile_ajax =function(url){
    return new Promise(function(resolve){
            $.ajax({
            url : url,
            type: 'POST',
                data: { get_param: 'value' },
            dataType: 'json',
        }).done(function(response){ //
            /* console.log(response); */
            resolve(response);
        });  
        
    })
}
var profile_ajax_with_send =function(serialized_data,url){//promesa que envia datos al conrolador segun la opcion
    return new Promise(function(resolve){
            $.ajax({
            url :url,
            type: 'POST',
            data : serialized_data,
            dataType: 'json',
        }).done(function(response){ //
            //console.log(response);
            resolve(response);
        });  
        
    })
}
function load_pages(option){ //carga las paginas dependiendo de cual este guardada en localstorage
        $(".profile_user_name").empty();
        $(".profile_user_mail").empty();
        $(".profile_user_avatar").empty();
         $("#tracking_social").empty();
         $("#purchase_history").empty();
    switch (option){
        case '#my_acount':
            /* console.log('cuenta'); */
            localStorage.setItem("profile_tab", option);
            load_my_acount();
        break;
        case '#update_profile':
            /* console.log('update'); */
            localStorage.setItem("profile_tab", option);
            
        break;
        case '#tracking_social':
            /* console.log('social'); */
            localStorage.setItem("profile_tab", option);
            load_my_media();
            
        break;
        case '#purchase_history':
            /* console.log('social'); */
            localStorage.setItem("profile_tab", option);
            load_purchase_history();
            
        break;
        default:
            /* console.log('default'); */
            localStorage.setItem("profile_tab", "#my_acount");
            load_my_acount();
        break;
    }

    var activeTab = localStorage.getItem('profile_tab');
    if(activeTab){
		$('#profile_tabs a[href="' + activeTab + '"]').tab('show');
	}
}
function load_my_acount(){
    profile_ajax("module/login_register/controller/profile_controller.php?op=user_profile_data").then(function(data_profile_my_acount){
        /* console.log(data_profile_my_acount[0]); */
        $(".profile_user_name").append(data_profile_my_acount[0].user_name);
        $(".profile_user_mail").append(data_profile_my_acount[0].user_mail);
        $(".profile_user_avatar").append("<img  src='"+data_profile_my_acount[0].avatar+"'>");
    })
}
function load_my_media(){
    url="module/login_register/controller/profile_controller.php?op=media_profile";
    profile_ajax(url).then(function(resolv) {
        /* console.log(resolv); */
        content="";
        content="<table id='example' class='display' style='width:100%'>";
        content=content+"<thead><tr><th>Modelo</th><th>Nombre</th><th>Accion</th></tr></thead>";
        content=content+"<tbody>";
        resolv.forEach(element => {
            content=content+"<tr><td>"+element.nombre+"</td><td>"+element.ms_name+"</td><td>"+element.typeOfAction+"</td></tr>";
        });  
        content=content+"</tbody>";
        content=content+"</table>";
        $("#tracking_social").append(content);
        if($("#example").length !=0){
            $('#example').DataTable();
        }
       
    })
}

function load_purchase_history() {
    profile_ajax("module/login_register/controller/profile_controller.php?op=all_purchase_history").then(function(resolv) {
        /* console.log(resolv); */
        content="";
        i=0;
        resolv.forEach(element => {
            /* console.log(element); */
            i=i+1;
            content="";
            content="<table id='example"+i+"' class='display' style='width:100%'>";  
            content=content+"<thead><tr><th>"+element.purchase_date+"</th><th></th><th>"+element.total_amount+"€</th></tr></thead>";
            content=content+"<tbody>";
           for (let index = 0; element[index]; index++) {
               /* console.log(element[index]); */
            content=content+"<tr><td>Product: "+element[index].ms_name+"</td><td>x"
                                                +element[index].cantidad+"</td><td>"+element[index].precio+"€</td></tr>";   
           }
           content=content+"</tbody>"; 
           content=content+"</table><br><br><br>";
            $("#purchase_history").append(content);
            if($("#example"+i).length !=0){
                $('#example'+i).DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]    
                } );
                
            }
        })
    })
       
}
function validate_new_mail(){
    if (!/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(document.getElementById('new_mail').value)){
        document.getElementById('e_Reg_new_email').innerHTML = "Tu Mail no es valido";
            return 1;
        }
        document.getElementById('e_Reg_new_email').innerHTML = "";
        return 0;
}
function validate_new_pass(){
    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(document.getElementById('new_pass').value)){
        document.getElementById('e_Reg_new_password').innerHTML = "Tu contraseña tiene que contener al menos 8 caracteres,una mayuscula, una minuscula y un caracter especial";
            return 1;
        }
        if(document.getElementById('re_new_pass').value==""){
            document.getElementById('e_Reg_new_password').innerHTML = "";
            return 1;
            }
        document.getElementById('e_Reg_new_password').innerHTML = "";
        return;
}
function confirm_new_pass(){
    if (document.getElementById('new_pass').value!=document.getElementById('re_new_pass').value||document.getElementById('new_pass').value==""){
        document.getElementById('e_Reg_new_confirm_password').innerHTML = "Tus contraseñas no coinciden";
            return 1;
        }
        document.getElementById('e_Reg_new_confirm_password').innerHTML = "";
        return 0;
}
function update_profile_data_controller(new_data){
    url="module/login_register/controller/profile_controller.php?op=update_control";
    profile_ajax_with_send(new_data,url).then(function(response){
        console.log(response.type);
        id=response.type;
        error=response.error;
        if(response!='ok'){
            document.getElementById(id).innerHTML = error;
        }else{
            load_pages("#my_acount");
            document.location.reload(true)
        }
    })
}
function habilitar_boton_update(ok){
     if(ok!=1){
        $('#update_profile_data').attr("disabled", false);//habilita boton
    }else{
        $('#update_profile_data').attr("disabled", true);//desabilita boton
    }
}

$(document).ready(function(){
    load_pages(localStorage.getItem("profile_tab"));//carga las paginas del perfil

   $('#profile_tabs').on('click','a',function (e) { // cambia las paginas cuando el usuario clike en ellas
       /* console.log($(this).attr('href')); */
        load_pages($(this).attr('href'));
    });

//validate new profile data
    var ok = 1; var ok2 = 1;
    $('#update_profile_data').attr("disabled", true);//desabilita boton
    $('#new_mail').on('focus blur change', function() {
            ok=validate_new_mail();
            habilitar_boton_update(ok);
    });
    $('#new_pass').on('focus blur change', function() {
            ok2=validate_new_pass();
            habilitar_boton_update(ok2);
    });
    $('#re_new_pass').on('focus blur change', function() {
            ok2=confirm_new_pass();
            habilitar_boton_update(ok2);
    });
    
    $('#update_profile_data').on('click', function(){
        new_profile_data = {mail:$('#new_mail').val(), 
                            new_pass:$('#new_pass').val(), 
                            re_new_pass:$('#re_new_pass').val()};
        update_profile_data_controller(new_profile_data);
    });
//validate new profile data

//cambiar foto de perfil
    $('#update_avatar').on('click',function (e) { //cuando se pulse en actualizar avatar
       profile_ajax("module/login_register/controller/profile_controller.php?op=update_avatar").then(function(resolve){
           //subir la imagen al servidor
           if(resolve==true){
               profile_ajax("module/login_register/controller/autentication_controller.php?op=renew_profile_pic").then(function(new_avatar){
                //recarga la pagina
                   setTimeout(() => {
                        load_pages("#my_acount");
                        document.location.reload(true);
                   }, 100);
               });
           }
       });
    });
//cambiar foto de perfil
})
