
$.ajax({ 
    type: 'GET', 
    url: "module/inicio/controller/controller_shop.php?op=load_b", 
    data: { get_param: 'value' }, 
    dataType: 'json'
})
.done(function( data, textStatus, jqXHR ) {
    $.each(data, function(i, item) {
        $('#bandos')
        .append($("<option value="+item.bando_id+">"+item.nombre+"</option>"));

   });
})
.fail(function( jqXHR, textStatus, errorThrown ) {
    if ( console && console.log ) {
        window.location.href='index.php?page=503';
        console.log( "La solicitud a fallado: " +  textStatus);
    }
});

$(document).ready(function(){
    $('#bandos').on('change',function(){
        var bando = $('#bandos').val();
       //console.log(bando);
        $.ajax({ 
            type: 'GET', 
            url: "module/inicio/controller/controller_shop.php?op=load_m&bando="+bando, 
            data: { get_param: 'value' }, 
            dataType: 'json'
        })
        .done(function( data, textStatus, jqXHR ) {
        //console.log(data);
        $('#modelos').empty();
        $('#modelos').append($("<option></option>"));
        $.each(data, function(i, item) {
            $('#modelos')
            .append($("<option value="+item.modelo_id+">"+item.nombre+"</option>"));

            });
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            if ( console && console.log ) {
                window.location.href='index.php?page=503';
                console.log( "La solicitud a fallado: " +  textStatus);
            }
        });
    });
    $('#modelos').on('change',function(){
        var modelo = $('#modelos').val();
       //console.log(modelo);
       var url= "module/inicio/controller/controller_shop.php?op=sugggestions_shop&modelo="+modelo; 
       // prepare the data
       var source =
       {
           datatype: "json",
           datafields: [
               { name: 'ms_modelo_id' },
               { name: 'ms_name' }
           ],
           url: url
       };
       var dataAdapter = new $.jqx.dataAdapter(source);
        // Create a jqxInput
       $("#jqxInput").jqxInput({ source: dataAdapter, 
        displayMember: "ms_name", 
        valueMember: "ms_name", height: 30,searchMode: 'containsignorecase'});
       $("#jqxInput").on('select', function (event) {
           if (event.args) {
               var item = event.args.item;
               if (item) {
                   var valueelement = $("<div></div>");
                   valueelement.text("value: " + item.value);
                   var labelelement = $("<div></div>");
                   labelelement.text("Label: " + item.label);
                   $("#jqxInput").children().remove();
                   $("#jqxInput").append(labelelement);
                   $("#jqxInput").append(valueelement);
               }
           }
       });
   });
});

    
