var home_ajax = function(url){
    return new Promise(function(resolve){
            $.ajax({
            url : url,
            type: 'POST',
            data: { get_param: 'value' },
            dataType: 'json',
        }).done(function(response){ //
            /* console.log(response); */
            resolve(response);
        });  
        
    })
}
function load_slider_products(current_page,modelo){
    url="module/inicio/controller/inicio.php?op=list_home_data_paginated&current_page="+current_page+"&modelo="+modelo;
    home_ajax(url).then(function(data){
            content="";
            if(data.length!=0){
                $.each(data, function(index, list) {
                    content +="<div class='item_pagination'>";
                    content += '<h4>'+list.Modelo+''+list.Nombre+'</h4>';
                    /* content=content+ "<div class='social like "+list.Modelo+" "+list.Nombre+"' ></div>"; */
                    content += "<a class='detailed_veiw_shop "+list.Nombre+"'><i class='fa fa-search'></i> </a>";
                    content=content+ "<i class='fa fa-plus' id='"+list.Modelo+" "+list.Nombre+"'></i></div>";
                });
                
            }
        /* console.log(data); */
         $('#list_featured_pag').empty();
        $('#list_featured_pag').append(content);
    })
}

$(document).ready(function(){
    var current_page        =   0;
    var loading             =   false;
    var oldscroll           =   0;
    console.log(current_page);
    load_slider_products(current_page,"no"); 
    
    $(document).on('click','#home_scroll',function(){
        console.log($(this).attr('class').split(' ')[0]);
        console.log(current_page);
        if($(this).attr('class').split(' ')[0]=="-"){
            if(current_page!=0){
                current_page--; 
            }
            
        }else{current_page++;}
        console.log(current_page);
        
        load_slider_products(current_page,"no"); 

    })
})

                    