function default_Ajax(url){
    $.ajax({ 
            type: 'GET', 
            url: url, 
            data: { get_param: 'value' }, 
            dataType: 'json',
        })
        .done(function( data, textStatus, jqXHR ) {
            //console.log(data.itemSummaries);
            resolve(data);
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            if ( console && console.log ) {
                console.log( "La solicitud a fallado: " +  textStatus);
                reject("Error");
            }
        });
}

function list_recomended_products(data){
    var content = "";
   if(data == "Error"){
       content = "NO se puede conectar a la Api"
   }else{
        content = '<ul class="listing">';
    $.each(data, function(index, list) {
        content += '<li><h4>'+list.title+'</h4>';
        content += '<div class="body"><img src="'+list.image.imageUrl+'"</div>';
        content += '<a class="cta '+index+'"><i class="fa fa-search"></i> </a></li>';
    });
    content += '</ul>';
   }
    return content;
}
var get_recomended_products = function(url,token) {
  return new Promise(function(resolve, reject) {
   $.ajax({ 
            type: 'GET', 
            url: url, 
            data: { get_param: 'value' }, 
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', api_key);
            }
        })
        .done(function( data, textStatus, jqXHR ) {
            //console.log(data.itemSummaries);
            resolve(data);
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            if ( console && console.log ) {
                console.log( "La solicitud a fallado: " +  textStatus);
                reject("Error");
            }
        });
  });
}


$(document).ready(function(){
    var content="";
    var content2="";
    var token = sessionStorage.getItem("api_token");
    var url = "https://api.ebay.com/buy/browse/v1/item_summary/search?q=Gunpla&limit=4";
    
        get_recomended_products(url,token)
            .then(function(result){
                sessionStorage.removeItem('content');
                 sessionStorage.removeItem('api_content');
                 //console.log(result.itemSummaries);
                 content=list_recomended_products(result.itemSummaries);
                 //console.log(content);
                 sessionStorage.setItem("api_content", content);
                 $('#list_api_shop').append(sessionStorage.getItem("api_content"));
        })
        .catch(function(result){
            content=list_recomended_products(result);
            sessionStorage.setItem("api_content", content);
            $('#list_api_shop').append(sessionStorage.getItem("api_content"));

        });
    $(document).on("click", ".cta",function() {
    sessionStorage.removeItem("content");
    console.log($(this).attr('class').split(' ')[1]);
    var name = $(this).attr('class').split(' ')[1];
    get_recomended_products(url,token)
    .then(function(result){     
        console.log(result.itemSummaries[name]);
        var list = result.itemSummaries[name];

        content2 += '<h3>'+list.title+'</h3>';
                content2 +="<div class='social like "+list.modelo+" "+list.ms_name+"'></div>";
                content2 += "<table id='table_detailed_shop'>";
                content2 += "<tr><td rowspan='12'><img src='"+list.image.imageUrl+"'></td>";
                content2 += "<td width=80><b>Seller</td>";
                content2 += '<td width=80><b>Item Location</td>';
                content2 += '<td width=80><b>Price</td>';
                content2 += '<td width=80><b>Action</td><tr>';
                content2 += '<tr><td width=80>'+list.seller.username+'</td>';
                content2 += '<td width=80>'+list.itemLocation.country+'</td>';
                content2 += '<td width=80>'+list.price.value+list.price.currency+'</td>';
                content2 += '<td width=80><a href="'+list.itemWebUrl+'" target="_blank">Comprar</a></td><tr>';
                content2 += "</table>";
        console.log(content2);
        sessionStorage.setItem("content", content2);
        $('#list_shop').append(sessionStorage.getItem("content")); 
        url = "index.php?page=shop";
        $( location ).attr("href", url);    
    });

});
});
