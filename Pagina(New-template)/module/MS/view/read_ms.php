<div class="gallery-top">
    <!-- container -->
    <div class="container">
        <div class="gallery-agileinfo">
            <h2 class="agileits-title">Informacion del Mobile Suit</h2>
        </div>
        <div class="gallery-w3agileits-row">
            <div class="gallery-w3grids">
                <p>
                <table border='2'>
                    <tr>
                        <td>Modelo: </td>
                        <td>
                            <?php
                                echo $ms['ms_modelo_id'];
                            ?>
                        </td>
                    </tr>
                
                    <tr>
                        <td>Nombre del modelo: </td>
                        <td>
                            <?php
                                echo $ms['ms_name'];
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Tamaños del MS: </td>
                        <td>
                            <?php
                                echo "Tamaño total: ".$ms['tamaño_Total']."-"."Tamaño sin equipo: ".$ms['tamaño'];
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Pesos del MS: </td>
                        <td>
                            <?php
                                echo "Peso total: ".$ms['peso_Total']."-"."Peso sin equipo: ".$ms['peso_Vacio'];
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Velocidad punta del MS: </td>
                        <td>
                            <?php
                                echo $ms['velocidad']."KMh";
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Autonomia: </td>
                        <td>
                            <?php
                                echo $ms['energia']."KW";
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Valocidad de encendido: </td>
                        <td>
                            <?php
                                echo $ms['encendido']."ms";
                            ?>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td>Alcance del sensor: </td>
                        <td>
                            <?php
                                echo $ms['rango_Sensor']."m";
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Numero de pilotos: </td>
                        <td>
                            <?php
                                echo $ms['pilots'];
                            ?>
                        </td>
                        </tr>


                    <tr>
                        <td>Precio por unidad: </td>
                        <td>
                            <?php
                                echo $ms['price_per_unit']."€";
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Inicio de su produccion: </td>
                        <td>
                            <?php
                                echo $ms['production_date'];
                            ?>
                        </td>
                    </tr>
                </table>
                </p>
                <input id="atras" name="Atras" type="button" value="Atras"/>
            </div>
        </div>
    </div>
    <!-- //container -->
</div>