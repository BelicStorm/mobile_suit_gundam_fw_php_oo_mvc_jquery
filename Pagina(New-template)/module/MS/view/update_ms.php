<div class="gallery-top">
    <!-- container -->
    <div class="container">
        <div class="gallery-agileinfo">
            <h2 class="agileits-title">Update</h2>
        </div>
        <div class="gallery-w3agileits-row">
            <div class="gallery-w3grids">
				<form method="post" name="form_ms" id="form_ms">
					<?php
					if(isset($error)){
						print ("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
					}?>

					<p>
					<label for="modelo">Modelo</label>
						<select name="modelo" >
							<option value="" selected>
							<option value="1">AMS
							<option value="2">AMX
							<option value="3">CCMS
							<option value="4">F71
							<option value="5">LM111E
							<option value="6">MS
							<option value="7">MSM
							<option value="8">MSS
							<option value="9">MSZ
	
							</select>
					<span id="e_modelo" class="styerror"></span>
				</p>
					<p>
						<label for="nombre">Nombre del modelo</label>
						<input name="nombre" id="nombre" type="text" placeholder="nombre" value="<?php echo $ms['ms_name'] ?>"  />
						<span id="e_nombre" class="styerror"></span>
					</p>
					<p>
						<label for="tamaño">Tamaño de la estructura principal</label>
						<input name="tamaño" id="tamaño" type="text" placeholder="0.0m" value="<?php echo $ms['tamaño'] ?>"  />
						<span id="e_tamaño" class="styerror"></span>
					</p>
					<p>
						<label for="tamaño_total">Tamaño total del Mobile Suit</label>
						<input name="tamaño_total" id="tamaño_total" type="text" placeholder="0.00m" value="<?php echo $ms['tamaño_Total'] ?>"  />
						<span id="e_tamaño_total" class="styerror"></span>
					</p>
					<p>
						<label for="peso_t">Peso total del MS</label>
						<input name="peso_t" id="peso_t" type="text" placeholder="0.0T" value="<?php echo $ms['peso_Total']?>"  />
						<span id="e_peso_t" class="styerror"></span>
					</p>
					<p>
						<label for="peso_vacio">Peso del Mobile Suit vacio</label>
						<input name="peso_vacio" id="peso_vacio" type="text" placeholder="0.0T" value="<?php echo $ms['peso_Vacio'] ?>" />
						<span id="e_peso_vacio" class="styerror"></span>
					</p>
					<p>
						<label for="velocidad">Velocidad punta del MS</label>
						<input name="velocidad" id="velocidad" type="text" placeholder="0.00km" value="<?php echo $ms['velocidad'] ?>"/>
						<span id="e_velocidad" class="styerror"></span>
					</p>
					<p>
						<label for="energia">Energia de la unidad</label>
						<input name="energia" id="energia" type="text" placeholder="0.00KW" value="<?php echo $ms['energia']?>"  />
						<span id="e_energia" class="styerror"></span>
					</p>
					<p>
						<label for="encendido">Volocidad de encendido</label>
						<input name="encendido" id="encendido" type="text" placeholder="0.0ms" value="<?php echo $ms['encendido'] ?>"  />
						<span id="e_encendido" class="styerror"></span>
					</p>
					<p>
						<label for="rango_sensor">Rango de los sensores oculares</label>
						<input name="rango_sensor" id="rango_sensor" type="text" placeholder="0.0m" value="<?php echo $ms['rango_Sensor'] ?>" />
						<span id="e_rango_sensor" class="styerror"></span>
					</p>
					<p>
						<label for="pilots">Numero de pilotos</label>
								<input type="radio" id="pilots" name="pilots" value="1" <?php if($ms['pilots']=="1") echo 'checked';?>/>1
								<input type="radio" id="pilots" name="pilots" value="2" <?php if($ms['pilots']=="2") echo 'checked';?> />2
						<span id="e_pilots" class="styerror"></span>
					</p>
					<p>
						<label for="price_per_unit">Precio por unidad</label>
						<input name="price_per_unit" id="price_per_unit" type="text" placeholder="0.0€" value="<?php echo $ms['price_per_unit'] ?>"  />
						<span id="e_price_per_unit" class="styerror"></span>
					</p>
					<p>
						<label for="date">Inicio de produccion</label>
						<input id="date" type="text" name="date"  value="<?php echo $ms['production_date'] ?>">
						<span id="e_date" class="styerror"></span>
					</p>
					<input id="update_ms" name="update_ms" type="button" value="Update"  />
					<input id="atras" name="Atras" type="button" value="Atras"/>
				</form>
            </div>
        </div>
    </div>
    <!-- //container -->
</div>
