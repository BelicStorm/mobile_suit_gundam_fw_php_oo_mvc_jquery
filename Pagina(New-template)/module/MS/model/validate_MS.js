//validate blur comprueba la perdida de foco y que el value del foco no este vacio
$(document).ready(function(){
	$('#nombre').on('focus blur change', function() {
		reg_nombre();
	});
	$('#tamaño').on('focus blur change', function() {
		reg_tamaño();
	});
	$('#tamaño_total').on('focus blur change', function() {
		reg_tamañoT();
	});
	$('#peso_t').on('focus blur change', function() {
		reg_peso_t();
	});
	$('#peso_vacio').on('focus blur change', function() {
		reg_peso_vacio();
	});
	$('#velocidad').on('focus blur change', function() {
		reg_velocidad();
	});
	$('#energia').on('focus blur change', function() {
		reg_energia();
	});
	$('#encendido').on('focus blur change', function() {
		reg_encendido();
	});
	$('#rango_sensor').on('focus blur change', function() {
		reg_rango_sensor();
	});
	$('#price_per_unit').on('focus blur change', function() {
		reg_precio_unidad();
	});
	   
});

	
	
//validate model comprueba que los campos no esten vacios antes de enviarse
function valida_ms_white(){
	var op = true;  
	$(document).ready(function(){
		$("input:radio").each(function(){
            var name = $(this).attr("name");
            if($("input:radio[name="+name+"]:checked").length == 0){
                op = false;
            }
        });
        
        if(!op){
			alert('Rellena los campos');
			return false;
        }
			$("#form_ms").find(':input').each(function() {
			var elemento= this;
				if(elemento.value.length==0){
					alert('Rellena los campos');
					op=false;
					if(op==false){
						return false;
					}
				}
			});
		});
		return op;
}
$(document).ready(function(){	
	$( "#create_ms").click(function() {
		if(valida_ms_white()){
			document.form_ms.submit();
			document.form_ms.action("index.php?page=controller_ms&op=create");
		}
		
	});
	$( "#update_ms").click(function() {
		if(valida_ms_white()){
			document.form_ms.submit();
			document.form_ms.action("index.php?page=controller_ms&op=update");
		}
	});
});
		



//funciones regx
function reg_nombre(){
	if (!/^[A-Z0-9][a-z0-9\/_].{0,10}$/.test(document.form_ms.nombre.value)){
		document.getElementById('e_nombre').innerHTML = "Entre 2 y 12 caracteres empezados por mayuscula o numero ";
		return false;
	}
	document.getElementById('e_nombre').innerHTML = "";
}
function reg_tamaño(){
	if (!/^([0-9]{1,2})+(\.[0-9]{1,2})?$/.test(document.form_ms.tamaño.value)){
		document.getElementById('e_tamaño').innerHTML = "Ejemplo 26";
		return false;
	}
	document.getElementById('e_tamaño').innerHTML = "";
}
function reg_tamañoT(){
	if (!/^([0-9]{1,2})+(\.[0-9]{1,2})?$/.test(document.form_ms.tamaño_total.value)){
		document.getElementById('e_tamaño_total').innerHTML = "Ejemplo 26";
		return false;
	}
	document.getElementById('e_tamaño_total').innerHTML = "";
}
function reg_peso_t(){
	if (!/([0-9]{1,2})+(\.[0-9]{1,3})?$/.test(document.form_ms.peso_t.value)){
		document.getElementById('e_peso_t').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_peso_t').innerHTML = "";
}
function reg_peso_vacio(){
	if (!/([0-9]{1,2})+(\.[0-9]{1,3})?$/.test(document.form_ms.peso_vacio.value)){
		document.getElementById('e_peso_vacio').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_peso_vacio').innerHTML = "";
}
function reg_velocidad(){
	if (!/([1-9])$/.test(document.form_ms.velocidad.value)){
		document.getElementById('e_velocidad').innerHTML = "Ejemplo 270";
		return false;
	}
	document.getElementById('e_velocidad').innerHTML = "";
}
function reg_energia(){
	if (!/([1-9])$/.test(document.form_ms.energia.value)){
		document.getElementById('e_energia').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_energia').innerHTML = "";
}
function reg_encendido(){
	if (!/([1-9])$/.test(document.form_ms.encendido.value)){
		document.getElementById('e_encendido').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_encendido').innerHTML = "";
}
function reg_rango_sensor(){
	if (!/([1-9])$/.test(document.form_ms.rango_sensor.value)){
		document.getElementById('e_rango_sensor').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_rango_sensor').innerHTML = "";
}
function reg_precio_unidad(){
	if (!/([0-9]{1,2})+(\.[0-9]{1,3})?$/.test(document.form_ms.price_per_unit.value)){
		document.getElementById('e_price_per_unit').innerHTML = "Ejemplo 456";
		return false;
	}
	document.getElementById('e_price_per_unit').innerHTML = "";
}
