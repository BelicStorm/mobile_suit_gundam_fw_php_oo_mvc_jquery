$(document).ready(function () {
    $('div#read_modal').load('module/MS/view/read_modal.html');
    //cargo el fichero y le asigno esta carga a un div en la pagina de lista
    $('body').on('click','.ms_m',function () {
        console.log(this.getAttribute('class').split(' ')[1]);
        var modelo = this.getAttribute('id');
        var name = this.getAttribute('name');
        var class2=this.getAttribute('class').split(' ')[1];
        //console.log(modelo);
        //console.log(name);
        console.log(class2);
        $.ajax({ 
            type: 'GET', 
            url: "module/MS/controller/controller_ms.php?op=read_modal&modelo=" + modelo + "&name=" + name, 
            data: { get_param: 'value' }, 
            dataType: 'json'
        })
        .done(function( data, textStatus, jqXHR ) {
            switch (class2) {
                case 'read':
                    read_modal(data);
                    break;
            
                default:
                    break;
            }
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            if ( console && console.log ) {
                window.location.href='index.php?page=503';
                console.log( "La solicitud a fallado: " +  textStatus);
            }
       });
    });
});

function modal_window(){
    $("#modal_hidden").show();
    $("#modal").dialog({
        width: 850, //<!-- ------------- ancho de la ventana -->
        height: 500, //<!--  ------------- altura de la ventana -->
        //show: "scale", <!-- ----------- animación de la ventana al aparecer -->
        //hide: "scale", <!-- ----------- animación al cerrar la ventana -->
        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
        //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }

        
    });
}
function read_modal(data){
    $("#product").html("<b>Producto:</b>"+" "+data.ms_modelo_id + " " + data.ms_name);
                $("#altura_ms").html("<b>Tamaños:</b></br>"+"Tamaño Total: "+data.tamaño_Total + " Tamaño sin equipo: " + data.tamaño);
                $("#kg_ms").html("<b>Pesos:</b></br>"+" Peso Total: "+data.peso_Total + " Peso Vacio " + data.peso_Vacio);
                $("#pilots").html("<b>Nomero de pilotos necesarios:</b>"+" "+data.pilots);
                $("#other").html("<b>Otras Caracteristicas:</b>");
                    $("#velocidad").html("<b>Velocidad Punta:</b>"+" "+data.velocidad);
                    $("#energia").html("<b>Energia de la unidad:</b>"+" "+data.energia);
                    $("#encencido").html("<b>Velocidad de encendido:</b>"+" "+data.encendido);
                    $("#sensor").html("<b>Rango del sensor:</b>"+" "+data.rango_Sensor);
                $("#production_start").html("<b>Inicio de su produccion:</b>"+" "+data.production_date);
                $("#price_per_unit").html("<b>Precio por unidad:</b>"+" "+data.price_per_unit);
                modal_window();
}