<div class="about">
		<div class="aboutw3-agileinfo">
			<!-- container -->
			<div class="container">
				<h2 data-tr="Conocenos" class="agileits-title"></h2>
			</div>
			<!-- //container -->
		</div>
		<!-- about-bottom -->
		<div class="about-bottom">
			<!-- container -->
			<div class="container">
				<div class="about-agileinfo">
					<div class="col-md-6 about-w3grid">
						<img src="view/images/s7.jpg" alt="" />
					</div>
					<div class="col-md-6 about-w3grid"> 
						<h5>Yoshiyuki industries es un tributo a la figura de Yoshiyuki Tomino creador de la franquicia Kidou Senshi Gundam.</h5>
						<p>Tomino comenzó su carrera en 1963 con el estudio Mushi Productions (Propiedad de Osamu Tezuka), escribiendo el guion 
                        de la que seria la primera serie animada japonesa, Tetsuwan Atomu (también conocida como Astro Boy). Posteriormente 
                        se convirtió en uno de los miembros más importantes de los estudios Sunrise,dirigiendo numerosas producciones durante las décadas de 1970s, 1980s y 1990s.
                        Tomino es famoso por haber transformado el género "Super Robot" del género mecha anime en "Real Robot" con la creación 
                        de Mobile Suit Gundam (1979), la primera serie de la franquicia homónima. Ha sido ganador de numerosos premios, incluyendo 
                        el de "Mejor Director" en 2006 durante el Tokyo International Anime Fair (Por sus compilaciones cinematográficas Mobile Suit Zeta Gundam: Heirs To The Stars en 2005).​ 
                        Dos series dirigidas por Tomino (Mobile Suit Gundam en 1979-80 y Space Runaway Ideon en 1980) ganaron el premio Anime Grand Prix de Animage.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!-- container -->
		</div>
		<!-- choose -->
		<div class="choose">
			<!-- container -->
			<div class="container">
				<h3>Universo Gundam</h3>
				<div class="choose-w3layoutsinfo">
					<div class="col-md-4 choose-grid">
						<h5>Universo Gundam</h5>
						<p>Gundam ,también conocida como Serie Gundam es el nombre colectivo de un 
                        conjunto de series animadas de ciencia ficción creadas por Yoshiyuki Tomino para los estudios Sunrise.</p>
						<div class="choose-button">
							<a href="https://gundam.fandom.com/es/wiki/Gundam_(franquicia)" target="_blank">More</a>
						</div>
					</div>
                    <div class="col-md-4 choose-grid">
						<h5>Universal Century</h5>
						<p>El Universal Century inicia después de que la humanidad empezara a poblar el espacio, después de que 
                        la construcción del grupo de colonias espaciales en Side 1 fuera completada.</p>
						<div class="choose-button">
							<a href="https://gundam.fandom.com/es/wiki/Universal_Century" target="_blank">More</a>
						</div>
					</div>
					<div class="col-md-4 choose-grid">
						<h5>Zeon</h5>
						<p>También conocido como el Ducado de Zeon, es una nación que aparece en Mobile Suit Gundam. Toma el
                        control de las colonias de Side 3 y lucha contra la Federación Terrestre durante la Guerra de un Año.</p>
						<div class="choose-button">
							<a href="https://gundam.fandom.com/es/wiki/Principado_de_Zeon" target="_blank">More</a>
						</div>
					</div>
                    <div class="col-md-4 choose-grid">
						<h5>Federacion Terrestre</h5>
						<p>La Federación de la Tierra (地球連邦 Chikyū Renpō) es un gobierno global presentado en la serie de televisión Mobile 
                        Suit Gundam. La Federación es una de las principales facciones de la línea de tiempo Universal Century.</p>
						<div class="choose-button">
							<a href="https://gundam.fandom.com/es/wiki/Federaci%C3%B3n_Terrestre" target="_blank">More</a>
						</div>
					</div>
                    <div class="col-md-4 choose-grid">
						<h5>Mobile Weapon</h5>
						<p>Mobile Weapon (機動兵器) es el término genérico usado para describir los vehículos que incorporan tecnología típicamente encontrada 
                        en mobile suits, en comparación con vehículos convencionales tales como tanques, aviones y barcos.</p>
						<div class="choose-button w3ls-btn">
							<a href="https://gundam.fandom.com/es/wiki/Mobile_Weapon" target="_blank">More</a>
						</div>
					</div>
					<div class="col-md-4 choose-grid">
						<h5>¿Que es un Gunpla?</h5>
						<p>Los Modelos Gundam son una línea de figuras de plastimodelismo producidas por Bandai. En ella se reproducen modelos a escala de robots,
                         vehículos y personajes de la serie Mobile Suit Gundam y sus secuelas. </p>
						<div class="choose-button w3ls-btn">
							<a href="https://es.wikipedia.org/wiki/Modelo_Gundam" target="_blank">More</a>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<!-- //container -->
		</div>
		<!-- //choose -->
	</div>