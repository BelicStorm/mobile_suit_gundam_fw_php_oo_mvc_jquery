<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

class common_sentence { 
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_all($tabla) {
        $arrArgument=$this->dao->select("*","$tabla");
       
    }
    public function select_with_where($tabla,$where) {
         $arrArgument=$this->dao->select("*","$tabla");
         $arrArgument=$this->dao->where_argument("$where");
    }
    /* public function simple_insert_into($tabla,$rows,$values){
        $this->dao->insert_into("*","$tabla");
        $this->dao->colums_insert_into("*","$tabla");
        $this->dao->values_insert_into("*","$tabla");
    } */
    public function simple_update($tabla,$where,$array) {
         $this->dao->update($tabla);
         foreach ($array as $key => $value) {
             if($key==0){
                 $this->dao->update_set($value["column"],$value["argument"]);
             }else{
                 $this->dao->more_update_set($value["column"],$value["argument"]);
             }
             
         }
         $this->dao->where_argument($where);
        /*  var_dump( $this->dao->content); */
    }
    /* $array=array(1=>array("column"=>,"argument"=>)...); */
}