<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/Pagina(New-template)/bd/';
if (!defined('SITE_ROOT')) define('SITE_ROOT', $path);
if (!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'conf_and_connection/');
require(MODEL_PATH . "db.class.singletone.php");
require(SITE_ROOT . "DAO/DAO.class.singletone.php");
require(SITE_ROOT . "utils/common_sentences.inc.php");

class shopList_bll { 
    private $dao;
    private $db;
    private $common;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
        $this->common = common_sentence::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function get_products($argument){
        $this->dao->select(" bandos.bando_id,
                      modelo.modelo_id,
                      bandos.nombre as bando,
                      modelo.nombre as modelo, 
                      ms_name,tamaño,
                      tamaño_Total,
                      peso_Total,peso_Vacio,
                      velocidad,energia,encendido,
                      rango_Sensor,pilots,price_per_unit,
                      production_date","mobile_suit");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument(" mobile_suit.ms_modelo_id = modelo.modelo_id");
        $this->dao->join_argument("bandos");
        $this->dao->on_argument("modelo.bando=bandos.bando_id");
        switch ($argument["action"]) {
            case 2:
                $sql = "bandos.bando_id=".$argument['bando'];
               break;
            case 3:
                $sql = "bandos.bando_id=".$argument['bando']." and modelo.modelo_id=".$argument['modelo'];
               break;
            case 4:
                $sql = "bandos.bando_id=".$argument['bando']." and modelo.modelo_id=".
                                        $argument['modelo']." and ms_name COLLATE UTF8_GENERAL_CI LIKE '%".$argument['name']."%'";
               break;
            case 5:
                $sql = "bandos.bando_id=".$argument['bando']."and ms_name COLLATE UTF8_GENERAL_CI LIKE '%".$argument['name']."%'";
               break;
            case 6:
                $sql = "ms_name COLLATE UTF8_GENERAL_CI LIKE '%".$argument['name']."%'";
               break;
       }
       if($argument["action"]!=1){
           $this->dao->where_argument($sql);
       }
        return $this->dao->get($this->db, $this->dao->content);
    }
}


       