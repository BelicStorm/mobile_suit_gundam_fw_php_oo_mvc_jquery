<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/Pagina(New-template)/bd/';
if (!defined('SITE_ROOT')) define('SITE_ROOT', $path);
if (!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'conf_and_connection/');
require(MODEL_PATH . "db.class.singletone.php");
require(SITE_ROOT . "DAO/DAO.class.singletone.php");
require(SITE_ROOT . "utils/common_sentences.inc.php");

class home_bll { 
    private $dao;
    private $db;
    private $common;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
        $this->common = common_sentence::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function list_products(){
        $this->dao->select("modelo.nombre as Modelo,
                            mobile_suit.ms_name as Nombre,
                            bandos.nombre as Productor","mobile_suit");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument("mobile_suit.ms_modelo_id = modelo.modelo_id");
        $this->dao->join_argument("bandos");
        $this->dao->on_argument("modelo.bando=bandos.bando_id");
        $this->dao->limit_argument(6);
        return $this->dao->get($this->db, $this->dao->content);
    }
    public function paginate_products($argument){

        $this->dao->select("modelo.nombre as Modelo,
                            mobile_suit.ms_name as Nombre,
                            bandos.nombre as Productor","mobile_suit");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument("mobile_suit.ms_modelo_id = modelo.modelo_id");
        $this->dao->join_argument("bandos");
        $this->dao->on_argument("modelo.bando=bandos.bando_id");
        $limit=$argument["limit"].",3";
        if($argument["modelo"]!="no"){
            $this->dao->where_argument('modelo.nombre='.$argument["modelo"]);
        }
        $this->dao->limit_argument($limit);
        return $this->dao->get($this->db, $this->dao->content);
    }

}