<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/Pagina(New-template)/bd/';
if (!defined('SITE_ROOT')) define('SITE_ROOT', $path);
if (!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'conf_and_connection/');

require(MODEL_PATH . "db.class.singletone.php");
require(SITE_ROOT . "DAO/DAO.class.singletone.php");
require(SITE_ROOT . "utils/common_sentences.inc.php");

class profile_bll { 
    private $dao;
    private $db;
    private $common;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
        $this->common = common_sentence::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
 
    public function select_all($table) {
        $this->common->select_all($table);
        return $this->dao->get($this->db, $this->dao->content);
    }
    public function select_user($name) {
        /* var_dump( $this->common->select_with_where("usuarios","user_name='$name'")); */
        $this->common->select_with_where("usuarios","user_name='$name'");
        return $this->dao->get($this->db, $this->dao->content);
    }
    public function update_profile($array){
        $name=$array[0];
        $this->common->simple_update("usuarios","user_name='$name'",$array[1]);
        return $this->dao->put($this->db, $this->dao->content);
    }
     public function select_media($name) {
        /* var_dump( $this->common->select_with_where("usuarios","user_name='$name'")); */
        $this->dao->select("usuarios.user_name,modelo.nombre,ms_name,typeOfAction","social_action_table");
        $this->dao->join_argument("usuarios");
        $this->dao->on_argument("social_action_table.user_id=usuarios.user_id");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument("social_action_table.ms_modelo_id=modelo.modelo_id");
        $this->dao->where_argument("usuarios.user_name='$name'");
        /* var_dump( $this->dao->content); */
        return $this->dao->get($this->db, $this->dao->content);
    }

}