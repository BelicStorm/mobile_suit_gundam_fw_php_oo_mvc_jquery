
<div class="header">
	<!-- container -->
	<div class="container">
		<div class="header-bottom">
			<div class="w3ls-logo">
				<h1>Yoshiyuki Industries</h1>
			</div>
			<div class="header-top-right">
				<div id="user_avatar"></div>
				<label id="logout">LogOut</label>
				<div id="cart">
					<!-- <i class="fa fa-shopping-cart" ></i> -->
					<div id="ex4"></span>
					</div>
				</div>
				
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="top-nav">
			<nav class="navbar navbar-default">
				<div class="container">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu						
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				<li class="home-icon"><a ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
					<li><a><span data-tr="Inicio" id="menu_home" name="menu_home"></a></li>
					<li><a><span data-tr="tienda_productos" id="menu_shop" name="menu_shop"></a></li>
					<li><a><span data-tr="Conocenos" id="menu_know" name="menu_know"></a></li>
					<li><a><span data-tr="Contacto" id="menu_contact" name="menu_contact"></a></li>
				</ul>	
				
			</div>		
			</nav>
	
		</div>
	</div>
	<!-- //container -->
</div>
<!-- //header -->
<!--Find Products-->
<div class="dad">
	<ul class="nav navbar-nav  agile"> 
		<li > 
			<select id="bandos" name="bandos">
				<option></option>
			</select>
		</li> 
		<li > 
			<select id="modelos" name="modelos">
				<option></option>
			</select>
		</li> 
	</ul> 
	<div class="autoguess"><!--Relativo-->
		<input type="text" id="jqxInput" />
	</div>
	<a class="search_autoguess"><i class="fa fa-search"></i> </a>
</div>
<!--Find Products-->
