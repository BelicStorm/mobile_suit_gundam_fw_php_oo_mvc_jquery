
window.onload= function(){
  cambiarIdioma();
  $(document).ready(function(){
    $("#btn-general-lang").on( "click", function(){
      var content = $("#lang").val();
      //console.log(content);
      cambiarIdioma(content);
    });
  });
};

function cambiarIdioma(lang){
  //console.log("Fichero cargado"+' menu_'+lang+'.json');
  // Habilita las 2 siguientes para guardar la preferencia.
  lang = lang || sessionStorage.getItem('app-lang') || 'es';
  sessionStorage.setItem('app-lang', lang);
  $.ajax({ 
    type: 'GET', 
    url: 'view/inc/lang/menu/menu_'+lang+'.json', 
    data: { get_param: 'value' }, 
    dataType: 'json',
    success: function (data) { 
        //console.log(data);
        var elems = document.querySelectorAll('[data-tr]');
        for (var x = 0; x < elems.length; x++) {
            elems[x].innerHTML = data.hasOwnProperty(lang)
                ? data[lang][elems[x].dataset.tr]
                : elems[x].dataset.tr;
        }
    }
  });
}
