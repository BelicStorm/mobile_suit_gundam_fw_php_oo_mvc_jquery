function delete_session_image(){
  $.ajax({
      url : "utils/upload_images.php?op=delete",
      type: 'POST',
      data: { get_param: 'value' },
      dataType: 'json',
  }).done(function(response){ //
      /* console.log(response); */
      
  }); 
}

$(function(){
Dropzone.autoDiscover = false;
// Dropzone class:
 var myDropzone = new Dropzone("div#mydropzone", {
    url:"utils/upload_images.php?op=pre_upload",//controlador de subidas
    maxFilesize: 5,//peso maximo del fichero
    maxFiles: 1,//numero maximo de imagenes por subida
    uploadMultiple: false,//prohibir la subida de multiples archivos
    addRemoveLinks: true,//añadir el delete files de forma grafica
    autoProcessQueue: true,//control de autosubida de archivos
    dictResponseError: 'Server not Configured',
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",//extensiones permitidas
    init:function(){
      var self = this;
      /* console.log(this.files.length); */
      if(this.files.length==0){delete_session_image();} //si no hay imagenes borra la sesion que las almacena y del servidor
      // config
      self.options.addRemoveLinks = true;
      self.options.dictRemoveFile = "Delete";//Texto del boton de borrado

      self.on("addedfile", function (file) { //cuando se añade un fichero
        console.log('new file added ', file);
      });

      self.on("sending", function (file) {//cuando se envia
        console.log('upload started', file);
        $('.meter').show();
      });
      
      self.on("totaluploadprogress", function (progress) { //progreso total de la subida
        console.log("progress ", progress);
        $('.roller').width(progress + '%');
      });

      self.on("queuecomplete", function (progress) {//subida completa
        $('.meter').delay(999).slideUp(999);
      });
      
      self.on("removedfile", function (file) {//fichero borrado
        console.log(file);
          delete_session_image(); //si se borra la imagen del dropzone, borra la imagen de la sesion que lo almacena y del servidor
      });
    }
  });
})
          